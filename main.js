/**Bài 1: Tìm số nguyên dương nhỏ nhất */
function soNguyenDuongNhoNhat() {
  var sum = 0;
  var n = 0;
  while (sum < 10000) {
    n = n + 1;
    sum = sum + n;
  }
  document.getElementById(
    "result1"
  ).innerHTML = `"Số nguyên dương nhỏ nhất là : ${n}`;
}

/**Bài 2: Tính tổng */

function tinhTong() {
  var x = document.getElementById("txt-so-x").value;
  var n = document.getElementById("txt-so-n").value;

  var sum = 0;
  var luyThua = 1;

  for (i = 1; i <= n; i++) {
    // moi lan lap gt tang len x lan
    luyThua *= x;
    // moi lan thuc hien vong lap bien sum se tang len luyThua
    sum += luyThua;
  }
  document.getElementById("result2").innerHTML = `Tổng: ${sum}`;
}

/**Bài 3: tính giai thừa */

function tinhGiaiThua() {
  // input number
  var nhapSo = document.getElementById("nhapSo").value;
  // output giaiThua=1
  var giaiThua = 1;

  var giaTri = 1;
  while (giaTri <= nhapSo) {
    giaiThua = giaiThua * giaTri;
    giaTri++;
  }
  document.getElementById("result3").innerHTML = giaiThua;
}


/**Bài 4: tạo thẻ div */

    
